const theme = {
  colors: {
    background: '#FEFEFE',
    text: '#303030',
    primary: '#00C9C8',
    secundary: '#f7007b',
    linkActive: '#00B0AF'
  }
}

export default theme